
#ifndef OUTPUT_CLASSES_HPP
#define OUTPUT_CLASSES_HPP

#include "ControlState.hpp"
#include "InputClasses.hpp"

class OutputClass
{
public:
  OutputClass()
  {}

  void show_output(const int output_pin, bool matched)
  {
    digitalWrite(output_pin, matched);
  }

};

 #endif
