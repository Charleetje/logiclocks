// Controlstate.hpp
// June 11th, 2019
// Charlee Fletterman

#ifndef CONTROL_STATE_HPP
#define CONTROL_STATE_HPP

#include "PuzzleTyp.hpp"

#include "InputClasses.hpp"

class ControlState
{
public:
  ControlState(const int color_index, double lower_boundary, double upper_boundary)
  : m_color(color_index)
  , m_lower_boundary(lower_boundary)
  , m_upper_boundary(upper_boundary)
  {
  }

  bool does_input_match_state(InputData& input)
  {
    return weight_matches_state(input.weight);
  }

  int get_color()
  {
    return m_color;
  }

  void set_is_solution(bool is_solution)
  {
    m_is_solution = is_solution;
  }
  bool get_is_solution()
  {
    return m_is_solution;
  }
private:
  bool weight_matches_state(double weight)
  {
    return ((weight > m_lower_boundary) && (weight <= m_upper_boundary));
  }

  const int m_color;
  double m_lower_boundary;
  double m_upper_boundary;
  bool m_is_solution;
};

#endif
