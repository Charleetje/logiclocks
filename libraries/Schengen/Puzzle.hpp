// Puzzle.hpp

#include <Arduino.h>

#include "ControlState.hpp"
#include "InputClasses.hpp"
#include "OutputClasses.hpp"

class Puzzle
{
public:
  Puzzle(InputClass* input,
         OutputClass* output,
         ControlState* states[3])
  : m_input(input)
  , m_output(output)
  {
    m_states[0] = states[0];
    m_states[1] = states[1];
    m_states[2] = states[2];
  }
  bool is_solved()
  {
    InputData data = m_input->get_input();
    Serial.println("Test");
    //m_logger.println("test");
    // int times_matched = 0;
    // for (int i=0; i<3; i++)
    // {
    //   if (m_states[i]->does_input_match_state(data))
    //   {
    //     m_output->show_output(i, 1);
    //     times_matched++;
    //   }
    //   else
    //   {
    //     m_output->show_output(i, 0);
    //   }
    return true;
    // }
  }

private:
  InputClass* m_input;
  OutputClass* m_output;
  ControlState* m_states[3];
};
