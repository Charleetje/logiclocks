
#ifndef INPUT_CLASSES_HPP
#define INPUT_CLASSES_HPP

#include "PuzzleTyp.hpp"
#include "HX711.h"

struct InputData
{
  double weight;
};

class WeightSensor
{
public:
  WeightSensor()
  {

  }
  void init(int dout_pin, int sck_pin)
  {
    m_hx.begin(dout_pin, sck_pin);
    long val = m_hx.read_average(10);
    Serial.println(val);
  }

  double get_weight()
  {
    long value = m_hx.read_average(10);
    Serial.print("Read value: ");
    Serial.println(value);
    return 0.0;
  }
  HX711 m_hx;
private:
  int m_dout_pin;
  int m_sck_pin;

};

class InputClass
{
public:
  InputClass()
  {

  }
  void init(int dout, int sck)
  {
    m_weight_sensor.init(dout, sck);
  }
  InputData get_input()
  {
    InputData data;
    //data.weight = m_weight_sensor->get_weight();

    //m_logger->println("potVal: ");
    // m_logger->print(data.weight);

    return data;
  }
private:
  WeightSensor m_weight_sensor;
};

#endif
