from flask_socketio import SocketIO
import time

from app import app

from gpio_communicator import GPIOCommunicator
from substatechecker import SubStateCheckThread, thread

NO_RFID_COUNTER = 10

app.config['SECRET_KEY'] = 'somesecretkey'
socketio = SocketIO(app)

gpio_communicator = GPIOCommunicator()
gpio_communicator.setup_pins()

class RFIDDoorInteraction(object):

    def __init__(self, gpioc):
        self.gpioc = gpioc
        self.reset()
        self.no_rfid_counter = 0

    def reset(self):
        print("Resetting")
        self.door_is_allowed_to_close = False

    def rfid_callback(self):
        time.sleep(0.25)
        if self.gpioc.is_rfid_present():
            self.no_rfid_counter = 0
            print("RFID present");
            if self.gpioc.is_rfid_correct():
                print("RFID correct")
                socketio.emit('rfid_read', {'solved' : True} )
            else:
                socketio.emit('rfid_read', {'solved' : False} )
        else:
            self.no_rfid_counter += 1
            if self.no_rfid_counter > NO_RFID_COUNTER:
                print("Emitting no RFID")
                socketio.emit('no_rfid_read', {})

    def door_callback(self):
        time.sleep(0.25)
        if self.gpioc.is_door_closed():
            if self.door_is_allowed_to_close:
                socketio.emit('door_state_change', {})
                self.gpioc.lock_door()

    def allow_door_close(self):
        self.door_is_allowed_to_close = True

rfid_door_interaction = RFIDDoorInteraction(gpio_communicator)
#gpio_communicator.set_door_state_callback(rfid_door_interaction.door_callback)

@socketio.on('connect')
def connect():
    print('Client connected')
    rfid_door_interaction.reset()

    thread = SubStateCheckThread(.5, {'rfid' : rfid_door_interaction.rfid_callback,
                                      'doorcallback': rfid_door_interaction.door_callback})
    thread.start()

@socketio.on('remove_rfid_reading')
def remove_rfid_reading():
    if thread.isAlive():
        thread.remove_callback('rfid')


@socketio.on('disconnect')
def disconnect():
    print ('Client disconnected')
    thread = None

@socketio.on('allow_door_close')
def allow_door_close():
    rfid_door_interaction.allow_door_close()

@socketio.on('release_door')
def release_door():
    gpio_communicator.release_door()
    rfid_door_interaction.reset()

@socketio.on('release_drawer')
def release_drawer():
    gpio_communicator.release_drawer()

@socketio.on('lock_drawer')
def lock_drawer():
    gpio_communicator.lock_drawer()

if __name__ == "__main__":
    socketio.run(app, debug=True)
