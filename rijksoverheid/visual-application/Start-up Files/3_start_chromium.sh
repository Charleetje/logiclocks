#!/bin/bash
# Disable alt keys
xmodmap -e "keycode 64="
xmodmap -e "keycode 108="

# Disable Contrl_L
xmodmap -e "keycode 37="

# Set "Menu" key to be F4 (Control_R+Menu is now chrome shutdown)
xmodmap -e "keycode 117=F4"

# Disable Menu 
xmodmap -e "keycode 135="

# Disable F keys
xmodmap -e "keycode 67=Escape"
xmodmap -e "keycode 68=Escape"
xmodmap -e "keycode 69=Escape"
xmodmap -e "keycode 70=Escape"
xmodmap -e "keycode 71=Escape"
xmodmap -e "keycode 72=Escape"
xmodmap -e "keycode 73=Escape"
xmodmap -e "keycode 74=Escape"
xmodmap -e "keycode 75=Escape"
xmodmap -e "keycode 76=Escape"
xmodmap -e "keycode 95=Escape"
xmodmap -e "keycode 96=Escape"

# Disable win keys
xmodmap -e "keycode 133="
xmodmap -e "keycode 134="

sed -i 's/"exited_cleanly":false/"exited_cleanly":true/' ~/.config/chromium/'Local State'
sed -i 's/"exited_cleanly":false/"exited_cleanly":true/; s/"exit_type":"[^"]\+"/"exit_type":"Normal"/' ~/.config/chromium/Default/Preferences
chromium-browser --disable-infobars --kiosk 'http://localhost:5000'

