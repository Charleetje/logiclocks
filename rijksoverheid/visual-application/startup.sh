#!/bin/bash

# bash script to start AI app
# to have script start up on startup, add the following line to /etc/xdg/lxsession/LXDE-pi/autostart
# @sh /home/pi/Documents/logiclocks/visual-application/startup.sh

lxterminal -e "/home/pi/Documents/logiclocks/rijksoverheid/visual-application/Start-up\ Files/1_start_vnc.sh; read -p 'Press enter to exit'" &
lxterminal -e "/home/pi/Documents/logiclocks/rijksoverheid/visual-application/Start-up\ Files/2_start_python.sh; read -p 'Press enter to exit'" &
lxterminal -e "/home/pi/Documents/logiclocks/rijksoverheid/visual-application/Start-up\ Files/3_start_chromium.sh; read -p 'Press enter to exit'" &

cd /
