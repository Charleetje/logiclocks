@startuml

class WeightSensor {
  get_current_weight()
}

class Lights {
  set_color(Color color)
}

class Display {
  set_message(**std::String&** msg)
}

class Puzzle {
  +start()
  +stop()
  +update_state()

  **ControlState*** m_previous_state
  **std::vector<ControlState>** m_states
}

class ControlState {
  **double** m_lower_boundary
  **double** m_upper_boundary
  **Color** m_light_color
  **Bool** m_has_message
  **std::String** m_message;

  +ControlState(**Color** color,\n  **double** lower_boundary,\n  **double** upper_boundary)
  +set_message(**std::String** message)
  +does_input_match_state(InputData& input) : **Bool**
  +get_color() : **Color**
  +get_message(**bool&** has_message) : **std::String**
  -weight_matches_state(**double** weight) : **Bool**
}

class InputData {
  **double** m_weight
}

class Input {
  - **WeightSensor** m_weight_sensor
  - InputData m_last_data

  -retrieve_weight()
  +get_input() : **InputData&**
}

class Output {
  **Lights** m_lights
  **Display** m_display
  do_output(**ControlState&** current_state)
}
InputData <-up- Input
WeightSensor <-up- Input
Lights <-down- Output
Display <-down- Output

Input <-up- Puzzle
Output <-down- Puzzle

Puzzle -right-> ControlState

@enduml
