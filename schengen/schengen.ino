#include "EEPROM.h"
#include "HX711.h"

const int PUZZLE_NUM = 4;
const int COLOR_NUM = 4;
const int BOUNDARY_NUM = 2;

#if defined(__AVR_ATmega2560__) // Compile with arduino MEGA pins
  const int PUZZLE_SOLVED = A11;
  const int TARE_BUTTON = A14;
  const int CAL_BUTTON = A13;
  const int SOLUTION_BUTTON = A15;
  const int puzzle_input_pins[PUZZLE_NUM][2] = {
      {A7, A8},{A3, A4}, {A5, A6}, {A1, A2}
    }; // SCK, DOUT
  const int puzzle_color_pins[PUZZLE_NUM][COLOR_NUM] = {
    {A9, 3, 4}, {5, 6, 7}, {8, 9, 10}, {11, 12, A10}
    }; // ORANGE, GREEN, RED
#else // Compile with pins available on arduino UNO
  const int PUZZLE_SOLVED = 13;
  const int TARE_BUTTON = 12;
  const int CAL_BUTTON = 12;
  const int SOLUTION_BUTTON = 12;
  const int puzzle_input_pins[PUZZLE_NUM][2] = {
      {A0, A1},{A0, A2}, {A0, A3}, {A0, A4}
    }; // SCK, DOUT
  const int puzzle_color_pins[PUZZLE_NUM][COLOR_NUM] = {
    {0, 1, 2}, {3, 4, 5}, {6, 7, 8}, {9, 10, 11}
    }; // ORANGE, GREEN, RED
#endif

const bool LIGHT_ON = LOW;
const bool LIGHT_OFF = HIGH;

const bool IGNORE_BUTTONS = false;
const bool do_puzzle = true;

const int SAMPLE_NUM = 5;
const double CAL_WEIGHT = 910000; // milligrams

double puzzle_boundaries[PUZZLE_NUM][COLOR_NUM][BOUNDARY_NUM];
double start_threshold[4] = {2.0, 2.0, 2.0, 3.0};
double allowed_error[4] = {50.0, 50.0, 50.0, 0.0};
double precision[4] = {4.0, 4.0, 4.0, 2.5};

const bool color_is_solution[4] = {false, true, false, false};

struct PuzzleCalibration
{
  // output
  float scale_factor;
  long int tare_factor;
  float solution_weight;
};

bool all_solved;

HX711* scales[PUZZLE_NUM];
double scale_factors[PUZZLE_NUM];
double tare_factors[PUZZLE_NUM];
PuzzleCalibration* puzzle_cals[PUZZLE_NUM];
int old_solution[PUZZLE_NUM];
int last_colors[PUZZLE_NUM];

void setup()
{
  Serial.begin(9600);
  Serial.println("Getting ready.....");
  setup_pins();

  for (int i=0; i<PUZZLE_NUM; ++i)
  {
    // Give some feedback about puzzle being setup
    activate_color(0, i);

    scales[i] = new HX711();
    scales[i]->begin(puzzle_input_pins[i][0], puzzle_input_pins[i][1]);

    puzzle_cals[i] = new PuzzleCalibration();
    EEPROM.get(i*sizeof(PuzzleCalibration), *puzzle_cals[i]);

    //Serial.print("Get tare factor");
    //get_tare_factor(i, *puzzle_cals[i]);

    set_boundaries(i);

    old_solution[i] = -1;

    // Feedback to user for finishing setup puzzle
    digitalWrite(puzzle_color_pins[i][0], LIGHT_ON);

    last_colors[i] = 3;
  }
}

void loop()
{
  check_input();
  Serial.println("%%%");

  all_solved = true;

  for (int i=0; i<PUZZLE_NUM; i++)
  {
    long int value = scales[i]->read_average(SAMPLE_NUM);
    Serial.print(value);
    double weight = get_weight_in_grams(value, puzzle_cals[i]->scale_factor, puzzle_cals[i]->tare_factor);
//    Serial.println(scales[i]->read());
    Serial.print(" -> Weight: ");
    Serial.print(weight);
    int color = get_current_color(i, weight);
    Serial.print(" ---------> color: ");
    Serial.print(color);
    Serial.print(" (solution: ");
    Serial.print(puzzle_cals[i]->solution_weight);
    Serial.println(")");

    activate_color(color, i);

    if (!color_is_solution[color] && !(color==-1 && color_is_solution[last_colors[i]]))
    {
      all_solved = false;
    }

    if (color!=-1)
    {
      last_colors[i] = color;
    }
  }

  if (all_solved)
  {
    digitalWrite(PUZZLE_SOLVED, LIGHT_ON);
    Serial.println("Puzzle was solved");
  }
  else
  {
    digitalWrite(PUZZLE_SOLVED, LIGHT_OFF);
    Serial.println("Puzzle was not solved");
  }
}

void setup_pins()
{
  pinMode(PUZZLE_SOLVED, OUTPUT);
  digitalWrite(PUZZLE_SOLVED, LIGHT_OFF);
  pinMode(TARE_BUTTON, INPUT);
  pinMode(CAL_BUTTON, INPUT);
  pinMode(SOLUTION_BUTTON, INPUT);
  digitalWrite(TARE_BUTTON, HIGH);
  digitalWrite(CAL_BUTTON, HIGH);
  digitalWrite(SOLUTION_BUTTON, HIGH);
  for (int i=0; i < PUZZLE_NUM; ++i)
  {
    for (int j=0; j < COLOR_NUM; ++j)
    {
      pinMode(puzzle_color_pins[i][j], OUTPUT);
      digitalWrite(puzzle_color_pins[i][j], LIGHT_OFF);
    }
  }
}

void write_all_colors(int puzzle, bool value)
{
  for (int j=0; j<COLOR_NUM; ++j)
  {
    pinMode(puzzle_color_pins[puzzle][j], OUTPUT);
    digitalWrite(puzzle_color_pins[puzzle][j], value);
  }
}

double get_weight_in_grams(long int val, double sc, double t)
{
//  Serial.print("Value: ");
//  Serial.println(val);
  return (sc*(val - t))/1000;
}

void get_scale_factor(HX711* scale, PuzzleCalibration& pinfo)
{
  Serial.println("Please put your calibration weight on the scales");
  delay(500);
  long int cal_value = scale->read_average(100);
  Serial.print("Calibration value: ");
  Serial.println(cal_value);
  pinfo.scale_factor = (CAL_WEIGHT)/(cal_value - pinfo.tare_factor);
  Serial.print("Scale factor: ");
  Serial.println(pinfo.scale_factor, 10);
}

void get_tare_factor(int i, PuzzleCalibration& pinfo)
{
  Serial.println("Please ensure the scales are empty");
//  delay(500);
  pinfo.tare_factor = scales[i]->read_average(20);
  Serial.print("Tare value: ");
  Serial.println(pinfo.tare_factor);
}

void get_solution_factor(int i, PuzzleCalibration& pinfo)
{
  Serial.println("Please make sure the solution is on the scales");
  delay(500);
  pinfo.solution_weight = get_weight_in_grams(scales[i]->read_average(20), puzzle_cals[i]->scale_factor, puzzle_cals[i]->tare_factor);
  Serial.print("Solution weight: ");
  Serial.println(pinfo.solution_weight);  
}

void set_boundaries(int i)
{
  // all lights off
  puzzle_boundaries[i][3][0] = -10000.0;
  puzzle_boundaries[i][3][1] = start_threshold[i] - precision[i];

  // solution boundary: green
  puzzle_boundaries[i][1][0] = puzzle_cals[i]->solution_weight - allowed_error[i] - precision[i];
  puzzle_boundaries[i][1][1] = puzzle_cals[i]->solution_weight + allowed_error[i] + precision[i];

  // too low: orange
  puzzle_boundaries[i][0][0] = start_threshold[i] + precision[i];
  puzzle_boundaries[i][0][1] = puzzle_boundaries[i][1][0] - precision[i];

  // too high: red
  puzzle_boundaries[i][2][0] = puzzle_boundaries[i][1][1] + precision[i];
  puzzle_boundaries[i][2][1] = 10000.0;

  Serial.print("Boundaries: (");
  Serial.print(puzzle_boundaries[i][3][0]);
  Serial.print(", ");
  Serial.print(puzzle_boundaries[i][3][1]);
  Serial.print(") (");
  Serial.print(puzzle_boundaries[i][0][0]);
  Serial.print(", ");
  Serial.print(puzzle_boundaries[i][0][1]);
  Serial.print(") (");
  Serial.print(puzzle_boundaries[i][1][0]);
  Serial.print(", ");
  Serial.print(puzzle_boundaries[i][1][1]);
  Serial.print(") (");
  Serial.print(puzzle_boundaries[i][2][0]);
  Serial.print(", ");
  Serial.print(puzzle_boundaries[i][2][1]);
  Serial.println(")");
}

void check_input()
{
  char serial_input = '0';
  bool tare_pressed = false;  
  bool cal_pressed = false;
  bool set_solution_pressed = false;

  if (Serial.available())
  {
    serial_input = Serial.read();
    Serial.print("Received serial input: ");
    Serial.println(serial_input);
  }

  tare_pressed = !digitalRead(TARE_BUTTON);
  cal_pressed = !digitalRead(CAL_BUTTON);
  set_solution_pressed = !digitalRead(SOLUTION_BUTTON);
  Serial.print("Button pressed (tare, cal, solution): (");
  Serial.print(tare_pressed);
  Serial.print(", ");
  Serial.print(cal_pressed);
  Serial.print(", ");
  Serial.print(set_solution_pressed);
  Serial.println(")");

  // if I just want to debug the buttons without the program constantly trying to calibrate etc.
  if (IGNORE_BUTTONS)
  {
    cal_pressed = false;
    tare_pressed = false;
    set_solution_pressed = false;
  }

  if ((serial_input == 't') || (tare_pressed))
  {
    for (int i=0; i < PUZZLE_NUM; ++i)
    {
      write_all_colors(i, LIGHT_OFF);
    }
    Serial.print("get tare factor");
    for (int i=0; i < PUZZLE_NUM; ++i)
    {
      activate_color(0, i);
      get_tare_factor(i, *puzzle_cals[i]);
      EEPROM.put(i*sizeof(PuzzleCalibration), *puzzle_cals[i]);
    }
    serial_input = '0';
    tare_pressed = false;
  }
  else if ((serial_input == 'c') || cal_pressed)
  {
    for (int i=0; i < PUZZLE_NUM; ++i)
    {
      write_all_colors(i, LIGHT_OFF);
    }
    for (int i=0; i < PUZZLE_NUM; ++i)
    {
      activate_color(1, i);
      get_scale_factor(scales[i], *puzzle_cals[i]);
      EEPROM.put(i*sizeof(PuzzleCalibration), *puzzle_cals[i]);
    }
    serial_input = '0';
    cal_pressed = false;
  }
  else if ((serial_input == 's') || set_solution_pressed)
  {
    for (int i=0; i < PUZZLE_NUM; ++i)
    {
      write_all_colors(i, LIGHT_OFF);
    }
    for (int i=0; i < PUZZLE_NUM; ++i)
    {
      activate_color(2, i);
      get_solution_factor(i, *puzzle_cals[i]);
      EEPROM.put(i*sizeof(PuzzleCalibration), *puzzle_cals[i]);
      set_boundaries(i);
    }
    serial_input = '0';
    set_solution_pressed = false;
  }
}

int get_current_color(int puzzle, double weight)
{
  int solution = -1;
  for (int i=0; i<COLOR_NUM; ++i)
  {
    if (weight > puzzle_boundaries[puzzle][i][0] && weight < puzzle_boundaries[puzzle][i][1])
    {
//      Serial.print("Found solution color: ");
//      Serial.println(i);
      solution = i;
      break;
    }
  }
  return solution;
}

void activate_color(int color, int puzzle_num)
{
  if (color != old_solution[puzzle_num] && color != -1)
  {
    // If the "solution" is equal to COLOR_NUM it means it is the "invalid range" at the bottom
    // -> in this case I still want to loop over all other colors so they are set to LOW
    for (int i=0; i<COLOR_NUM-1; i++)
    {
      if (i==color)
      {
        digitalWrite(puzzle_color_pins[puzzle_num][i], LIGHT_ON);
      }
      else
      {
        digitalWrite(puzzle_color_pins[puzzle_num][i], LIGHT_OFF);
      }
    }
  }
}
