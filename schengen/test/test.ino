#include "HX711.h"
//
//// HX711 circuit wiring
const int LOADCELL_DOUT_PIN = 2;
const int LOADCELL_SCK_PIN = 3;
const int LOADCELL_DOUT_PIN2 = 4;
const int LOADCELL_SCK_PIN2 = 5;

HX711 scale;
long zero_offset;
long scale_factor;
long zero_offset2;
long scale_factor2;

HX711 scale2;

void setup() {
  Serial.begin(9600);
  Serial.println("Getting ready....");
  scale.set_logger(&Serial);
  scale2.set_logger(&Serial);
  scale.begin(LOADCELL_DOUT_PIN, LOADCELL_SCK_PIN);
  scale2.begin(LOADCELL_DOUT_PIN2, LOADCELL_SCK_PIN2);

  zero_offset = scale.read_average(10);
  zero_offset2 = scale2.read_average(10);
  Serial.print("Zero offset: ");
  Serial.println(zero_offset);
  Serial.print("Zero offset2: ");
  Serial.println(zero_offset2);
  scale_factor = 50;
  scale_factor2 = 100;
}

void loop() {
  //Serial.println("Loop..");
  if (scale.is_ready()) {
    long reading = scale.read_average(10);
    long reading2 = scale2.read_average(10);
    Serial.print("HX711 reading: ");
    Serial.println(abs(reading - zero_offset)/scale_factor);
    Serial.print("HX711 reading2: ");
    Serial.println(abs(reading2 - zero_offset2)/scale_factor2);
  } else {
    Serial.println("HX711 not found.");
  }

  delay(3000);
  
}
